<?php

namespace Tests\Feature;

use Tests\TestCase;
use WP_Query;

class AcfSearchableQueryTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->factory->post->create_many(10); // just to mess around
        $this->factory->post->create(['post_title' => 'ipsum']);
    }

    /** @test */
    public function it_will_return_results_if_db_fields_is_searchable()
    {
        $this->acfDbField('text', 'text_field', ['acf_searchable' => true]);
        $post = $this->newPostWithAcf(['text_field' => 'ipsum']);

        $query = new WP_Query(['s' => 'ipsum']);

        $this->assertCount(2, $query->posts);
        $this->assertContains($post->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function it_will_not_return_results_if_db_fields_is_not_searchable()
    {
        $this->acfDbField('text', 'text_field', ['acf_searchable' => false]);
        $post = $this->newPostWithAcf(['text_field' => 'ipsum']);

        $query = new WP_Query(['s' => 'ipsum']);

        $this->assertCount(1, $query->posts);
        $this->assertNotContains($post->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function it_will_return_results_if_db_fields_is_not_set_and_default_is_true()
    {
        remove_action('acf/update_field', [$GLOBALS['acf_searchable_option'], 'updateSearchableSetting'], 20);
        $field = $this->acfDbField('text', 'text_field');
        $post = $this->newPostWithAcf(['text_field' => 'ipsum']);

        $query = new WP_Query(['s' => 'ipsum']);

        $this->assertArrayNotHasKey('acf_searchable', $field);
        $this->assertCount(2, $query->posts);
        $this->assertContains($post->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function it_will_not_return_results_if_db_fields_is_set_but_field_type_is_disabled()
    {
        add_filter('acf-searchable/enabled_field_types', function() { return ['textarea']; });
        $this->acfDbField('text', 'text_field', ['acf_searchable' => true]);
        $post = $this->newPostWithAcf(['text_field' => 'ipsum']);

        $query = new WP_Query(['s' => 'ipsum']);

        $this->assertCount(1, $query->posts);
        $this->assertNotContains($post->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function it_will_return_results_if_local_fields_is_searchable()
    {
        $this->acfLocalField('text', 'text_field', ['acf_searchable' => true]);
        $post = $this->newPostWithAcf(['text_field' => 'ipsum']);

        $query = new WP_Query(['s' => 'ipsum']);

        $this->assertCount(2, $query->posts);
        $this->assertContains($post->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function it_will_not_return_results_if_local_fields_is_not_searchable()
    {
        $this->acfLocalField('text', 'text_field', ['acf_searchable' => false]);
        $post = $this->newPostWithAcf(['text_field' => 'ipsum']);

        $query = new WP_Query(['s' => 'ipsum']);

        $this->assertCount(1, $query->posts);
        $this->assertNotContains($post->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function it_will_return_results_if_local_fields_is_not_set_and_default_is_true()
    {
        remove_action('acf/update_field', [$GLOBALS['acf_searchable_option'], 'updateSearchableSetting'], 20);
        $field = $this->acfLocalField('text', 'text_field');
        $post = $this->newPostWithAcf(['text_field' => 'ipsum']);

        $query = new WP_Query(['s' => 'ipsum']);

        $this->assertArrayNotHasKey('acf_searchable', $field);
        $this->assertCount(2, $query->posts);
        $this->assertContains($post->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function it_will_not_return_results_if_local_fields_is_set_but_field_type_is_disabled()
    {
        add_filter('acf-searchable/enabled_field_types', function() { return ['textarea']; });
        $this->acfLocalField('text', 'text_field', ['acf_searchable' => true]);
        $post = $this->newPostWithAcf(['text_field' => 'ipsum']);

        $query = new WP_Query(['s' => 'ipsum']);

        $this->assertCount(1, $query->posts);
        $this->assertNotContains($post->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function it_will_not_include_results_if_acf_searchable_is_globally_turned_off()
    {
        add_filter('acf-searchable/auto_apply_to_wp_query', '__return_false');
        $this->acfLocalField('text', 'text_field', ['acf_searchable' => true]);
        $post = $this->newPostWithAcf(['text_field' => 'ipsum']);

        $query = new WP_Query(['s' => 'ipsum']);

        $this->assertCount(1, $query->posts);
        $this->assertNotContains($post->ID, wp_list_pluck($query->posts, 'ID'));
    }

    /** @test */
    public function it_will_not_include_results_if_acf_searchable_is_turned_off_by_query_args()
    {
        $this->acfLocalField('text', 'text_field', ['acf_searchable' => true]);
        $post = $this->newPostWithAcf(['text_field' => 'ipsum']);

        $query = new WP_Query(['s' => 'ipsum', 'disable_acf_search' => true]);

        $this->assertCount(1, $query->posts);
        $this->assertNotContains($post->ID, wp_list_pluck($query->posts, 'ID'));
    }

    // navitive search

    /** @test */
    public function it_will_respect_wordpress_default_search_order()
    {
        $this->acfLocalField('text', 'text_field', ['acf_searchable' => true]);
        $post1 = $this->factory->post->create(['post_title' => 'The quick brown fox jumps over the lazy dog']); // 1
        $post2 = $this->factory->post->create(['post_title' => 'quick brown fox with lazy dog jumps over']); // 2
        $post3 = $this->factory->post->create(['post_title' => 'quick fox can jumps']); // 3
        $post4 = $this->factory->post->create(['post_excerpt' => 'The quick brown fox jumps over the lazy dog']); // 4
        $post5 = $this->factory->post->create(['post_excerpt' => 'quick brown fox with lazy dog jumps over']); // 6
        $post6 = $this->factory->post->create(['post_excerpt' => 'quick fox can jumps']); // 6
        $post7 = $this->factory->post->create(['post_content' => 'The quick brown fox jumps over the lazy dog']); // 5
        $post8 = $this->factory->post->create(['post_content' => 'quick brown fox with lazy dog jumps over']); // 6
        $post9 = $this->factory->post->create(['post_content' => 'quick fox can jumps']); // 6
        $post10 = $this->newPostWithAcf(['text_field' => 'The quick brown fox jumps over the lazy dog'])->ID; // 4
        $post11 = $this->newPostWithAcf(['text_field' => 'quick brown fox with lazy dog jumps over'])->ID; // 5
        $post12 = $this->newPostWithAcf(['text_field' => 'quick fox can jumps'])->ID; // 6

        $query = new WP_Query([
            's' => 'fox jumps',
            'posts_per_page' => -1
        ]);

        $postIds = wp_list_pluck($query->posts, 'ID');

        $this->assertArrayOrderAfter($post1, [$post10, $post11, $post12], $postIds);
        $this->assertArrayOrderAfter($post2, [$post10, $post11, $post12], $postIds);
        $this->assertArrayOrderAfter($post3, [$post10, $post11, $post12], $postIds);

        $this->assertArrayOrderAfter($post10, [$post5, $post6, $post7, $post8, $post9], $postIds);
        $this->assertArrayOrderAfter($post11, [$post5, $post6, $post8, $post9], $postIds);
    }
}
