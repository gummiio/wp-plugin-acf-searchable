<?php

namespace Tests\Unit;

use Tests\TestCase;
use WP_Query;

class AcfSearchableQueryTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        add_filter('acf-searchable/enabled_field_types', function() { return ['text', 'textarea']; });
        add_filter('acf-searchable/default_searchable_field_types', function() { return ['text']; });

        $this->query = new WP_Query([]);
        $this->searchQuery = new WP_Query(['s' => 'ipsum']);
    }

    /** @test */
    public function distinct_will_not_be_added_if_no_acf_fields_need_to_be_searched()
    {
        add_filter('acf-searchable/enabled_field_types', '__return_empty_array');

        $this->assertEquals(
            apply_filters('posts_distinct', '', $this->query),
            apply_filters('posts_distinct', '', $this->searchQuery)
        );
    }

    /** @test */
    public function distinct_will_be_added_if_acf_fields_need_to_be_searched()
    {
        $this->assertEquals('DISTINCT', apply_filters('posts_distinct', '', $this->searchQuery));
    }

    /** @test */
    public function join_will_not_be_added_if_no_acf_fields_need_to_be_searched()
    {
        add_filter('acf-searchable/enabled_field_types', '__return_empty_array');

        $this->assertEquals(
            apply_filters('posts_join', '', $this->query),
            apply_filters('posts_join', '', $this->searchQuery)
        );
    }

    /** @test */
    public function join_will_be_added_if_acf_fields_need_to_be_searched()
    {
        $this->assertNotEquals(
            apply_filters('posts_join', '', $this->query),
            $acfSearchJoin = apply_filters('posts_join', '', $this->searchQuery)
        );
        $this->assertRegExp('/acf_ref/', $acfSearchJoin);
        $this->assertRegExp('/acf_field/', $acfSearchJoin);
        $this->assertRegExp('/acf_value/', $acfSearchJoin);
        $this->assertRegExp('/LIKE \'%s:4:"type";s:4:"text";%\' AND acf_field.post_content LIKE \'%s:14:"acf_searchable";%:1;%\'/', $acfSearchJoin);
    }

    /** @test */
    public function additional_join_will_be_added_if_field_type_defaulat_is_true()
    {
        $acfSearchJoin = apply_filters('posts_join', '', $this->searchQuery);
        $this->assertNotRegExp('/LIKE \'%s:4:"type";s:4:"textarea";%\' AND acf_field.post_content NOT LIKE \'%s:14:"acf_searchable";%\'/', $acfSearchJoin);
        $this->assertRegExp('/LIKE \'%s:4:"type";s:4:"text";%\' AND acf_field.post_content NOT LIKE \'%s:14:"acf_searchable";%\'/', $acfSearchJoin);
    }

    /** @test */
    public function search_will_not_be_added_if_no_acf_fields_need_to_be_searched()
    {
        add_filter('acf-searchable/enabled_field_types', '__return_empty_array');

        $this->assertEquals(
            apply_filters('posts_search', '', $this->query),
            apply_filters('posts_search', '', $this->searchQuery)
        );
    }

    /** @test */
    public function search_will_be_added_if_acf_fields_need_to_be_searched()
    {
        $this->assertNotEquals(
            apply_filters('posts_search', '', $this->query),
            $acfSearchWhere = apply_filters('posts_search', '', $this->searchQuery)
        );
        $this->assertRegExp('/acf_value\.meta_key = SUBSTRING\(acf_ref\.meta_key, 2\)/', $acfSearchWhere);
    }

    /** @test */
    public function local_field_search_will_be_added_if_local_fields_include_searchable_field_type()
    {
        $this->acfLocalField('text', 'text_field');
        $query = new WP_Query(['s' => 'ipsum']);

        $acfSearchWhere = apply_filters('posts_search', '', $query);
        $this->assertRegExp('/acf_ref.meta_value IN\([^\)]+\)/', $acfSearchWhere);
    }

    /** @test */
    public function local_field_search_will_not_be_added_if_local_fields_did_not_include_searchable_field_type()
    {
        $this->acfLocalField('radio', 'radio_field');
        $query = new WP_Query(['s' => 'ipsum']);

        $acfSearchWhere = apply_filters('posts_search', '', $query);
        $this->assertNotRegExp('/acf_ref.meta_value IN\([^\)]+\)/', $acfSearchWhere);
    }

    /** @test */
    public function nagative_search_will_also_be_parsed_properly()
    {
        $query = new WP_Query(['s' => '-ipsum lorem']);

        $acfSearchWhere = apply_filters('posts_search', '', $query);
        $this->assertRegExp('/acf_value\.meta_value NOT LIKE \'\{[^\}]+\}ipsum{[^\}]+\}\'/', $acfSearchWhere);
        $this->assertRegExp('/acf_value\.meta_value LIKE \'{[^\}]+\}lorem{[^\}]+\}\'/', $acfSearchWhere);
    }

    /** @test */
    public function orderby_will_not_be_added_if_no_acf_fields_need_to_be_searched()
    {
        add_filter('acf-searchable/enabled_field_types', '__return_empty_array');

        $this->assertEquals(
            apply_filters('posts_search_orderby', '', $this->query),
            apply_filters('posts_search_orderby', '', $this->searchQuery)
        );
    }

    /** @test */
    public function orderby_will_not_be_added_if_only_one_term_is_searched()
    {
        $this->assertEquals(
            apply_filters('posts_search_orderby', '', $this->query),
            $acfSearchOrderBy = apply_filters('posts_search_orderby', '', $this->searchQuery)
        );
        $this->assertNotRegExp('/WHEN acf_value.meta_value LIKE \'\{[^\}]+\}ipsum\{[^\}]+\}\' THEN 3/', $acfSearchOrderBy);
    }

    /** @test */
    public function orderby_will_be_added_if_more_than_one_term_is_searched()
    {
        $query = new WP_Query(['s' => 'lorem ipsum']);

        // have to hack the default a bit for filter to work
        $this->assertNotEquals(
            apply_filters('posts_search_orderby', ' ELSE 6 END', $this->query),
            $acfSearchOrderBy = apply_filters('posts_search_orderby', ' ELSE 6 END', $query)
        );
        $this->assertRegExp('/WHEN acf_value.meta_value LIKE \'\{[^\}]+\}lorem ipsum\{[^\}]+\}\' THEN 4/', $acfSearchOrderBy);
    }

    /** @test */
    public function acf_search_query_injection_can_be_disabled_by_filter()
    {
        add_filter('acf-searchable/auto_apply_to_wp_query', '__return_false');

        $this->assertEquals(
            apply_filters('posts_distinct', '', $this->query),
            apply_filters('posts_distinct', '', $this->searchQuery)
        );

        $this->assertEquals(
            apply_filters('posts_join', '', $this->query),
            apply_filters('posts_join', '', $this->searchQuery)
        );

        $this->assertEquals(
            apply_filters('posts_search', '', $this->query),
            apply_filters('posts_search', '', $this->searchQuery)
        );

        $this->assertEquals(
            apply_filters('posts_search_orderby', '', $this->query),
            apply_filters('posts_search_orderby', '', $this->searchQuery)
        );
    }

    /** @test */
    public function acf_search_query_injection_can_be_disabled_by_query_args()
    {
        $searchQuery = new WP_Query(['s' => 'ipsum', 'disable_acf_search' => true]);

        $this->assertEquals(
            apply_filters('posts_distinct', '', $this->query),
            apply_filters('posts_distinct', '', $searchQuery)
        );

        $this->assertEquals(
            apply_filters('posts_join', '', $this->query),
            apply_filters('posts_join', '', $searchQuery)
        );

        $this->assertEquals(
            apply_filters('posts_search', '', $this->query),
            apply_filters('posts_search', '', $searchQuery)
        );

        $this->assertEquals(
            apply_filters('posts_search_orderby', '', $this->query),
            apply_filters('posts_search_orderby', '', $searchQuery)
        );
    }
}
