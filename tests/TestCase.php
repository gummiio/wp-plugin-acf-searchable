<?php

namespace Tests;

use Carbon\Carbon;
use Symfony\Component\DomCrawler\Crawler;
use Tests\AcfSupports;

class TestCase extends \WP_UnitTestCase
{
    use AcfSupports;

    public function setUp()
    {
        parent::setUp();

        $this->acfClearLocals();
    }

    protected function newPostWithAcf($fields = [], $args = [])
    {
        $post = $this->factory->post->create_and_get($args);

        foreach ($fields as $name => $value) {
            update_field($name, $value, $post);
        }

        return $post;
    }

    protected function assertArrayOrderAfter($reference, $target, $array)
    {
        if (! is_array($target)) $target = [$target];

        $referenceIndex =  array_search($reference, $array);

        foreach ($target as $t) {
            $targetIndex = array_search($t, $array);

            $this->assertTrue(
                $targetIndex > $referenceIndex,
                sprintf('Cannot assert "%s"(%d) is after "%s"(%d).', $t, $targetIndex, $reference, $referenceIndex)
            );
        }
    }

    protected function assertArrayOrderBefore($reference, $target, $array)
    {
        if (! is_array($target)) $target = [$target];

        $referenceIndex =  array_search($reference, $array);

        foreach ($target as $t) {
            $targetIndex = array_search($t, $array);

            $this->assertTrue(
                $targetIndex > $referenceIndex,
                sprintf('Cannot assert "%s"(%d) is after "%s"(%d).', $t, $targetIndex, $reference, $referenceIndex)
            );
        }
    }
}
