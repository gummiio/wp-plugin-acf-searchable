<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb5b02d5dff08fb6b6d44410e1d5acd8c
{
    public static $files = array (
        '7ae3120effce7d599483032fd91368cc' => __DIR__ . '/../..' . '/src/helpers/helpers.php',
    );

    public static $prefixLengthsPsr4 = array (
        'G' => 
        array (
            'Gummiforweb\\AcfSearchable\\' => 26,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Gummiforweb\\AcfSearchable\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Gummiforweb\\AcfSearchable\\AcfSearchableOption' => __DIR__ . '/../..' . '/src/AcfSearchableOption.php',
        'Gummiforweb\\AcfSearchable\\AcfSearchableQuery' => __DIR__ . '/../..' . '/src/AcfSearchableQuery.php',
        'Gummiforweb\\AcfSearchable\\SearchableCore' => __DIR__ . '/../..' . '/src/SearchableCore.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb5b02d5dff08fb6b6d44410e1d5acd8c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb5b02d5dff08fb6b6d44410e1d5acd8c::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitb5b02d5dff08fb6b6d44410e1d5acd8c::$classMap;

        }, null, ClassLoader::class);
    }
}
