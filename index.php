<?php
/*
    Plugin Name: Advanced Custom Fields: Searchable
    Plugin URI: https://acf-searchble.gummi.io/
    Description: Acvanced Custom Fields add on. Enable searchable options for individual field when doing a regular wp search.
    Version: 1.0.0
    Author: Alan Chen
    License: GPLv2 or later
    License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

defined('ABSPATH') or die('No script kiddies please!');

define('ACF_SEARCHABLE_FILE', __FILE__);
define('ACF_SEARCHABLE_PATH', dirname(__FILE__));
define('ACF_SEARCHABLE_URL', plugins_url('', __FILE__));

require_once ACF_SEARCHABLE_PATH . '/vendor/autoload.php';

$GLOBALS['acf_searechable'] = (new \Gummiforweb\AcfSearchable\SearchableCore)->init();
