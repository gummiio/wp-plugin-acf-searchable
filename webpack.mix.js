let mix = require('laravel-mix');

mix.setPublicPath("./");
mix.sass('assets/scss/acf-searchable-field-group.scss', 'assets/css/')
   .js('assets/js/acf-searchable-field-group.js', 'assets/js/acf-searchable-field-group.min.js')
   .options({processCssUrls: false});
