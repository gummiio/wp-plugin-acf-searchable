(function($) {
    if (typeof(acf) == 'undefined') return;

    acf.add_action('ready', function($el) {
        $('.acf-switch-input[id$="-acf_searchable"]').trigger('change');
    });

    acf.add_action('append', function($el) {
        $('.acf-switch-input[id$="-acf_searchable"]', $el).trigger('change');
    });

    $(document).on('change', '.acf-switch-input[id$="-acf_searchable"]', function() {
        var $el = $(this);
        var $field = $el.closest('.acf-field-object');

        if ($el.is(':checked')) {
            $field.addClass('acf-is-searchable');
        } else {
            $field.removeClass('acf-is-searchable');
        }
    });
})(jQuery);
