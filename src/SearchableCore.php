<?php

namespace Gummiforweb\AcfSearchable;

use Gummiforweb\AcfSearchable\AcfSearchableOption;
use Gummiforweb\AcfSearchable\AcfSearchableQuery;

class SearchableCore
{
    public function init()
    {
        add_action('plugins_loaded', [$this, 'loadTextdomain']);
        add_action('acf/init', [$this, 'loadSearchableOption']);
        add_action('acf/init', [$this, 'loadSearchableQuery']);

        return $this;
    }

    public function loadTextdomain()
    {
        load_plugin_textdomain('acf-searchable', false, ACF_SEARCHABLE_PATH . '/assets/lang/');
    }

    public function loadSearchableOption()
    {
        $GLOBALS['acf_searchable_option'] = new AcfSearchableOption;
    }

    public function loadSearchableQuery()
    {
        $GLOBALS['acf_searchable_query'] = new AcfSearchableQuery;
    }
}
