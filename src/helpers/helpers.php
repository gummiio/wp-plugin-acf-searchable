<?php

function acf_searchable_get_enabled_field_types() {
    $field_types = apply_filters('acf-searchable/enabled_field_types', [
        'select', 'text', 'textarea', 'wysiwyg'
    ]);

    return array_intersect($field_types, array_keys(acf()->fields->types));
}

function acf_searchable_get_default_searchable_field_types() {
    $field_types = apply_filters('acf-searchable/default_searchable_field_types', [
        'text', 'textarea', 'wysiwyg'
    ]);

    return array_intersect($field_types, array_keys(acf()->fields->types));
}

function acf_searchable_is_field_type_enabled($fieldType) {
    return in_array($fieldType, acf_searchable_get_enabled_field_types());
}

function acf_searchable_is_field_type_searchable($fieldType) {
    return in_array($fieldType, acf_searchable_get_default_searchable_field_types());
}
