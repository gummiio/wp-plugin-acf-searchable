# WordPress Plugin - Advanced Custom Field: Searchable

Enable searchable toggle on individual acf field.

[ ![Bitbucket Pipeline Status for gummiio/wp-plugin-acf-searchable](https://bitbucket-badges.atlassian.io/badge/gummiio/wp-plugin-acf-searchable.svg)](https://bitbucket.org/gummiio/wp-plugin-acf-searchable/overview)
